% First comes an example EPS file -- just ignore it and
% proceed on the \documentclass line
% your LaTeX will extract the file if required
\begin{filecontents*}{example.eps}
%!PS-Adobe-3.0 EPSF-3.0
%%BoundingBox: 19 19 221 221
%%CreationDate: Mon Sep 29 1997
%%Creator: programmed by hand (JK)
%%EndComments
gsave
newpath
  20 20 moveto
  20 220 lineto
  220 220 lineto
  220 20 lineto
closepath n
2 setlinewidth
gsave
  .4 setgray fill
grestore
stroke
grestore
\end{filecontents*}
%
\RequirePackage{fix-cm}

\documentclass[twocolumn,referee]{svjour3}          % [twocolumn] normaal voeg 'draft' toe om slechte boxes aan te tonen en voeg 'referee' toe om veel plaats te laten tss de lijnen

\smartqed  % flush right qed marks, e.g. at end of proof

\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{nicefrac}
\usepackage{cite}

% \usepackage{mathptmx}      % use Times fonts if available on your TeX system

% Insert the name of "your journal" with
\journalname{Analog Integrated Circuits and Signal Processing}

\begin{document}

\title{Finding the dominant source of distortion in two-stage op-amps\thanks{This work is sponsored by the Vrije Universiteit Brussel, dept. ELEC, Pleinlaan 2, 1050 Brussels, Belgium, Fund for Scientific Research (FWO-Vlaanderen), Institute for the Promotion of Innovation through Science and Technology in Flanders (IWT-Vlaanderen), the Flemish Government (Methusalem), the Belgian Federal Government (IUAP VI/4) and the Strategic Research Program of the VUB (SRP-19)}}
%\titlerunning{Short form of title}        % if too long for running head

\author{Adam Cooman \and Gerd Vandersteen}

\authorrunning{A. Cooman and G. Vandersteen} % if too long for running head

\institute{A. Cooman \at  \email{acooman@vub.be}}

\date{Received: 15-01-2013 / Accepted: date}
% The correct dates will be entered by the editor

\maketitle

\begin{abstract} % abstract 150 - 250 words
Non-linear distortion levels are an important part of an analog circuit's specification. Using the usual linear design flow, non-linear performance is determined last and little clues about the sources of distortion are provided. Recently, the Best Linear Approximation (BLA) has been introduced to approximate non-linear systems. It allows to describe the behaviour of a non-linear system as a linear Frequency Response Function combined with a coloured noise source to describe the distortion. To determine the dominant source of non-linear distortion, we combined the BLA with a classical noise analysis. This paper explains the BLA-based noise analysis and shows the result of the simulation-based analysis when applied to various op-amp architectures. The analysis can be used to effectively decrease the amount of non-linear distortion generated by the op-amp, since it pinpoints the non-linear hot-spots in an efficient way.
\keywords{non-linear distortion \and operational amplifiers}
\end{abstract}

\section{Introduction}
\label{sec:Intro}

% context/need
Most design flows for analog/RF circuits rely only on linear time invariant reasoning. When the linear design flow is completed, the importance of non-linearities is assessed by identifying compression points and/or intercept points. Those points provide a measure of the non-linear behaviour of the total circuit but don't show the relative importance of the different sources of non-linear distortion. If specifications are not met, the designer needs to be able to pinpoint the source of the problem to solve it efficiently. \\

% task
In \cite{Wambacq1998,Hernes2005,Cannizzaro2006} a Volterra-based approach was used to localize the dominant non-linearity of the circuit in an analytic way. For larger circuits, this analytic approach yields lengthy, complex expressions. Overview is hence easily lost. These Volterra-based methods also require the replacement of the transistor model by an approximate analytic non-linear model. \\

Recently, the Best Linear Approximation (BLA) for non-linear systems has been developed \cite{Pintelon2012chapter3}[MEER REFS NAAR BLA PAPERS]. It describes the behaviour of a non-linear system when the system is excited with an input signal that has a certain Power Spectral Density (PSD) and distribution. The BLA approximates the dynamic non-linear system by a linear Frequency Response Function (FRF) with a coloured noise source to model the non-linear distortion. It can be extracted quickly by performing transient simulations when exciting the circuit with a multisine excitation. No special transistor models are required here. \\

The noise-like properties of the non-linear distortion (proven by using the BLA\cite{Pintelon2012chapter3}) enable the use of a classical noise analysis on the distortion sources. This new BLA-based noise analysis allows to determine the dominant source of non-linear distortion in a circuit, using plain transient simulations without the use of special transistor models or complicated analytical expressions.\\

% object
This paper explains the necessary steps needed to pinpoint the dominant source of non-linear distortion in a two-stage op-amp. It starts by looking a bit deeper into the theory behind the method. First, the benefits of multisine excitations in the context of non-linear systems are explained (Section \ref{sec:multisine}). Then, the BLA is introduced and third (Section \ref{sec:BLA}), the combination of noise analysis and BLA is explained (Section \ref{sec:theory}). The second part of the paper shows the results of the method when it is applied to several single-ended op-amps and to a fully differential op-amp (Section \ref{sec:Miller}).

\section{Multisine excitation}
\label{sec:multisine}

The non-linear distortion generated by a circuit depends not only on the circuit itself, but also on the power spectral density (PSD) and the amplitude distribution in the time domain (PDF) of the signal applied to the circuit \cite{Schetsen1980}. The test signals used to determine the distortion of the circuit should therefore have similar PSD and PDF as the signals in the final application of the circuit. If the circuit is built to handle Gaussian-distributed signals, Gaussian-distributed excitation signals are needed. This  makes random Gaussian noise a popular excitation signal for the estimation of non-linear circuits \cite{Schetsen1980}.\\

It is impossible to separate the distortion from the measurement noise when using random excitation signals. This is due to the fact that distortion behaves like noise itself \cite{Pintelon2012chapter3}. Besides this limitation, random signals also suffer from leakage and possibly a reduced signal to noise ratio (SNR) at some frequencies. These properties make random signals less suitable for the easy detection and qualification of distortion in analog circuits.\\

Periodic excitation signals allow to separate the non-linear distortion from the noise if the non-linear system is Period In Same Period Out (PISPO)\footnote{Limiting the class of systems under consideration to PISPO systems excludes systems with bifurcations and chaotic systems. Systems with dynamic saturation or discontinuous non-linearities are still PISPO}. The non-linear distortion generated by a PISPO system is periodic. It can therefore be separated from the random noise. By properly setting up the experiment, leakage can be avoided when computing the spectral response of a PISPO system on a periodic excitation.\\

\begin{figure}
  \includegraphics[width=\columnwidth]{Prentjes/excitationedit.pdf}
% figure caption is below the figure
\caption{Different Gaussian-distributed excitation signals. The random Gaussian noise (filtered white noise on top) suffers from leakage and doesn't allow to separate distortion from noise. The periodic Gaussian noise (middle) doesn't allow full control over the PSD. The multisine (bottom) combines the best of both worlds.}
\label{fig:MultisinesVSGaussian}
\end{figure}

Even for the periodic signals, we still want the excitation signal to have the same PSD and PDF as the signal used in the application of the circuit. This requirement means that one- or two-tone signals will not always be sufficient. An obvious choice is to use a periodic repetition of a Gaussian distributed noise realisation (Figure \ref{fig:MultisinesVSGaussian}). By doing this, control over the PSD of the used signal is low. Using multisines is a better alternative. \\

A multisine is a signal which consists of a sum of $N$ harmonically related sines
\begin{equation}
s(t)=\frac{1}{\sqrt{N}} \sum\limits_{k=1}^N A_k sin \left( 2 \pi k f_0 t + \phi_k \right)
\label{eq:multisine}
\end{equation}
where $A_k$ and $\phi_k$ are the amplitude and phase of the $k^\mathrm{th}$ spectral line and $f_0$ is the frequency resolution of the multisine.
A multisine is called a random-phase multisine if the values of the phases $\phi_k$ are drawn from a uniform distribution  over $\left[ 0 , 2\pi \right[$. If the random-phase multisine contains enough tones ($N$ sufficiently large), it can be shown that the random-phase multisine has a Gaussian distribution \cite{Schoukens2009}.\\

The multisine is the ideal excitation signal to determine the distortion generated by the non-linear system under test. 
\begin{itemize}
	\item The PSD of the multisine can be set to resemble the PSD of the signals present in the application of the circuit.
	\item The PDF is Gaussian, so it will resemble a lot of the real-world signals.
	\item The signal is periodic, enabling the separation of the non-linear distortion and noise.
\end{itemize}

Carefully choosing the location of the tones of the multisine allows to extract even more information about the non-linearities of the circuit. Consider an odd random-phase multisine which is a random-phase multisine that excites odd frequency lines only ($f_k=0$ for all $k$ even). Even order non-linearities will then always combine an even number of frequencies in the excitation signal. Because all excited frequencies are odd, only contributions at even frequency lines ($f_k$ even) will occur (Figure \ref{fig:NLresponse}c). For odd non-linearities, the story is similar: Odd non-linearities combine an odd number of frequencies, so the odd non-linear contributions will fall at odd frequency lines ($f_k$ odd, Figure \ref{fig:NLresponse}d).\\

The fact that the even non-linear contributions will only fall on even frequency lines and the odd non-linear contributions on odd frequency lines allows us to split the even and odd non-linear contributions in one single experiment. To be able to split the odd non-linear contributions from the linear response to the multisine, several odd frequencies can be left unexcited. This excitation signal is called a random-odd random-phase multisine and is shown in Figure \ref{fig:NLresponse}a. The response at these odd `detection lines' can only consist of odd non-linear contributions and hence give an indication of the amount of odd non-linear distortion generated by the circuit.\\

\begin{figure}
\begin{center}
  \includegraphics[width=\columnwidth]{Prentjes/NLresponse.pdf}
% figure caption is below the figure
\caption{Response of a non-linear system to an random-odd random-phase multisine. The odd and even non-linear contributions can easily be separated by looking at the odd and even frequency lines respectively}
\label{fig:NLresponse}
\end{center}
\end{figure}

If feedback is present, or the system contains a cascade of non-linear systems, the signal at the input of every stage will no longer be a clean odd random-phase multisine. This will deteriorate the separation between odd and even non-linear distortion contributions. The linear response at the excited frequency lines will be a lot higher than the distortion if the system is dominantly linear. The distortion added by system can then be estimated by performing a linear correction on the output spectrum (see later in section \ref{subsec:BLAFromMeas}).

\section{Best Linear Approximation}
\label{sec:BLA}

Applying several phase realisations of the multisine to the circuit enables the extraction of the Best Linear Approximation (BLA). The BLA approximates the response of the system, to signals with the same PSD and PDF as the multisines used to determine it, by a linear time-invariant system with an additive coloured noise source to the output. To understand the effect of the averaging over different phase realisations of the multisine, we should first look into the behaviour of non-linear distortion components.

\subsection{Coherent and non-coherent contributions}

When we apply a random-odd random-phase multisine to a PISPO non-linear system, the response $Y$  at a given frequency $f_{k}$ can be written as\footnote{Lets ignore that we are able to split the even and odd order non-linear response of the non-linear system with an odd multisine for now.}
\begin{equation}
Y_M(f_{k})=Y^{[1]}(f_{k})+Y^{[2]}(f_{k})+Y^{[3]}(f_{k})+\ldots+N_{Y}(f_{k}) 
\label{eq:FullResponse}
\end{equation}
where $Y^{[i]}$ is the $\mathrm{i}^{\mathrm{th}}$  order response of the non-linear system and $N_{Y}$  is the noise. The first order response is given by the linear transfer function:
\begin{equation*}
Y^{[1]}(f_{k})=G(f_{k})\cdot U(f_{k}) 
\end{equation*}
For the second order, the response term is given by applying the second order Volterra kernel on the input signal:
\begin{equation}
Y^{[2]}(f_{k})=\sum_{i=-N}^{N}H^{[2]}(f_{i},f_{k}-f_{i}) \cdot U(f_{i}) \cdot U(f_{k}-f_{i}) 
\label{eq:Volterra2}
\end{equation}
The third order response
\begin{equation}
\begin{split}
Y^{[3]}(f_{k})=\sum_{i=-N}^{N} &\sum_{j=-N}^{N} H^{[3]}(f_{i},f_{j},f_{k}-f_{i}-f_{j}) \cdot \\
&U(f_{i}) \cdot U(f_{j}) \cdot U(f_{k}-f_{i}-f_{j}) 
\end{split}
\label{eq:Volterra3}
\end{equation}
can be split into two different contributions
\begin{itemize}
	\item The coherent contributions $Y_{coh}^{[3]}$. %Their phase depends only on the phase of the frequency component in the input signal at $f_{k}$ and the Volterra kernel.
	\item The non-coherent contributions $Y_{ncoh}^{[3]}$, % for which the phase depends on the phase of more than one frequency component in the input signal together with the phase of the Volterra kernel.
\end{itemize}
\begin{equation*}
Y^{[3]}=Y_{coh}^{[3]}+Y_{ncoh}^{[3]}
\end{equation*}
The coherent contributions of the third order response are obtained when a frequency and its negative counterpart are combined through the third order non-linearity: 
\begin{equation*}
\begin{split}
Y_{coh}^{[3]}(f_{k})&=\sum_{i=-N}^{N}H^{[3]}(f_{i},-f_{i},f_{k}-f_{i}+f_{i}) \cdot \\
&\hphantom{=\sum_{i=-N}^{N}} U(f_{i})\cdot U(-f_{i}) \cdot U(f_{k}-f_{i}+f_{i})\\
&=\sum_{i=-N}^{N}H^{[3]}(f_{i},-f_{i},f_{k})U(f_{k}) \cdot \left|U(f_{i})\right|^{2}\\
&=U(f_{k})\sum_{i=-N}^{N}H^{[3]}(f_{i},-f_{i},f_{k}) \cdot \left|U(f_{i})\right|^{2}
\end{split}
\end{equation*}
The phase of this component only depends on the phase of the Volterra Kernel and on the phase of $U(f_k)$, because the magnitude squared of a complex number $U(f_i)$ is a real number. Dividing the coherent contribution at frequency $f_{k}$  by $U(f_{k})$ results in
\begin{equation*}
\frac{Y_{coh}^{[3]}(f_{k})}{U(f_{k})}=\sum_{i=-N}^{N}H^{[3]}(f_{i},-f_{i},f_{k}) \cdot \left|U(f_{i})\right|^{2}\triangleq G_{coh}^{[3]}(f_k) 
\end{equation*}
The phase of $G_{coh}^{[3]}(f_k)$ only depends on the Volterra kernel. This why the contributions are called coherent contributions. The non-coherent contributions are all the other components in the double sum of (\ref{eq:Volterra3}). It can be shown that every odd-order non-linear response can be separated into coherent and non-coherent contributions:
\begin{equation*}
Y^{[i]}=Y_{coh}^{[i]}+Y_{ncoh}^{[i]}\,\mathrm{for\, all\, i\, odd}
\end{equation*}
Looking at (\ref{eq:Volterra2}), it can be seen that no coherent contributions can be present in the second-order response if no DC is present in the input signal. Hence
\begin{equation*}
Y^{[i]}=Y_{ncoh}^{[i]}\,\mathrm{for\, all\, i\, even} 
\end{equation*}
Following these conclusions, we can rewrite the response of the non-linear system (\ref{eq:FullResponse}) as\footnote{for readability, the frequency dependency has been left out.}
\begin{equation*}
\begin{split}
Y_M=&Y^{[1]}+Y_{coh}^{[3]}+Y_{coh}^{[5]}+\ldots \\
&+Y_{ncoh}^{[2]}+Y_{ncoh}^{[3]}+Y_{ncoh}^{[4]}+Y_{ncoh}^{[5]}+\ldots+N_{Y}
\end{split}
\end{equation*}
Hence, we obtain
\begin{equation*}
\begin{split}
\frac{Y_M(f_{k})}{U_M(f_{k})}=&\underbrace{\vphantom{\frac{Y_{ncoh}^{[2]}}{U}}G}_{G_{0}}+\underbrace{\vphantom{\frac{Y_{ncoh}^{[2]}}{U}}G_{coh}^{[3]}+G_{coh}^{[5]}+\ldots}_{G_{B}}+\underbrace{\frac{N_{Y}}{U}}_{G_{N}}\\
&+\underbrace{\frac{Y_{ncoh}^{[2]}}{U}+\frac{Y_{ncoh}^{[3]}}{U}+\frac{Y_{ncoh}^{[4]}}{U}+\frac{Y_{ncoh}^{[5]}}{U}+\ldots}_{G_{S}}
\end{split}
\end{equation*}

The Best Linear Approximation (BLA) of the non-linear system is then defined as
\begin{equation}
G_{BLA}=G_{0}+G_{B}
\label{eq:BLA}
\end{equation}

with $G_0$ the underlying linear system and $G_B$ a bias term. $G_B$ can represent compression, expansion or descentitisation of the non-linear system. $G_{BLA}$ acts like the FRF of a linear system when the system is excited with signals that have a similar PSD and PDF as the signal used to determine the BLA.\\

The remaining term $G_S$ is uncorrelated with the input signal \cite{Pintelon2012chapter3} and can hence be considered as noise. This leads to the description of the behaviour of the non-linear system as a linear system $G_{BLA}$ with a noise source $G_S$ added to the output (Figure \ref{fig:BLA}).

\begin{figure}
\includegraphics[width=\columnwidth]{Prentjes/BLA.pdf}
\caption{Using the BLA, we can model the behaviour of a non-linear system as a linear system where the distortion is modelled as a noise source}
\label{fig:BLA}
\end{figure}

\subsection{Obtaining the BLA from measurements}
\label{subsec:BLAFromMeas}

To obtain the BLA, we need to separate the coherent, the non-coherent and the noise contributions. The analysis described in this paper is simulation-based and hence noiseless. Expressions to determine the BLA from noisy measurements are found in \cite{Pintelon2004a}. The separation of the coherent and the non-coherent contributions is done by applying several random odd random phase multisines with different phase realisations (but the same frequency grid and PSD).\\

%\begin{itemize}
%\item The phase of the term $G_{0}$, which is the phase of the linear transfer function, will not change when a different phase realisation of the multisine is applied.
%\item By definition, the phase of the coherent contributions $G_B$ does't change when another phase realisation of the multisine is applied.
%\item The phase of the non-coherent contributions depends on the phase of two or more frequency components in the input signal. With a different phase realisation comes a new phase relation between those components, so the phase of the components $G_{S}$ will change when a different phase realisation is applied to the system. It can be shown that $G_{S}$ behaves like circular complex normally distributed noise when different phase realisations are considered. Its mean is zero \cite{Pintelon2012chapter3}.
%\end{itemize}

By averaging over different phase realisations, the term $G_{S}$  will average out. What remains after this averaging is an estimate of the BLA. If the measurements are noiseless, we can also determine the RMS value of the non-coherent contributions by looking at the standard deviation of the output signal at non-excited frequency lines over different phase realisations. This standard deviation is a measure for the amount of distortion introduced by the non-linear system.\\
% BLA in feedback
The op-amps we consider in this paper are placed in their feedback configuration. Due to the feedback, the op-amp input contains distortion generated at the output. The signal found there is not a random-odd random-phase multisine any more. It is still possible to determine the Best Linear Approximation though. The non-coherent contributions remain uncorrelated with the multisine, so the noise-like properties of the distortion are not lost. Determining the BLA is done in the same way as before, For the distortion however, we have to compensate for the signal present at the input at unexcited frequency lines. Since we know the BLA of the system, we can perform a first-order correction of the output to obtain the distortion introduced by the stage itself.
\begin{equation}
G_S = Y_M - G_{BLA} \cdot U_M
\label{eq:spectralcorrection}
\end{equation}
Where $Y_M$ is the measured output spectrum and $U_M$ the measured input spectrum. The BLA is only known at excited frequency lines, so to perform this spectral correction on non-excited frequency lines, a first-order interpolation of the BLA has to be performed.

\section{Finding the dominant source of non-linear distortion using the BLA}
\label{sec:theory}
% Combine the BLA with noise analysis to obtain the analysis method

The non-coherent non-linear contributions behave like noise. This means we can apply a classical noise analysis on them to be able to compare the different sources of non-linear distortion in a circuit. We will refer the non-coherent contributions to the output node of the op-amp, where their relative influence can be compared.

To determine the noise introduced by a stage at the output node, we need
\begin{itemize}
\item The PSD of the noise source
\item The FRF from that noise source to the output node.
\end{itemize}

\begin{figure}
\begin{center}
	\includegraphics[width=0.8\columnwidth]{Prentjes/transient.pdf}
\end{center}
\caption{Simulation set-up for the transient simulation. The signals that should be saved are shown in red}
\label{fig:transientsim}       % Give a unique label
\end{figure}

To determine the PSD of the noise source, we need to determine the PSD of the non-coherent contributions introduced by the stage using (\ref{eq:spectralcorrection}). The BLA of the stage is needed, so we perform several transient simulations with different phase realisations of a multisine that represents the class of signals the op-amp is made for. The simulation set-up is shown in Figure \ref{fig:transientsim}. The input and output signals of both stages are saved and transformed into the frequency domain. Averaging in the frequency domain over the different phase realisations gives the BLA of the stages. Applying (\ref{eq:spectralcorrection}) to stage 1 of the op-amp (Figure \ref{fig:transientsim}), we obtain
\begin{equation*}
G_{S,1}=I_{int}-G_{BLA,1} \cdot V_{amp}
\end{equation*}
Where $G_{S,1}$ are the non-coherent contributions introduced by stage 1 and $G_{BLA,1}$ is the Best Linear Approximation of the first stage. The non-coherent contributions of stage 2 ($G_{S,2}$) can be obtained in a similar way.\\

In a two-stage op-amp, the input and output impedance of the stages is not infinite. To be completely accurate, a two-port representation of the stages should be used, leading to the BLA for Multiple Input Multiple Output (MIMO) systems \cite{Dobrowiecki2007}. This complicates everything and it will be shown that accurate results can be obtained without using the full MIMO implementation. A few extra assumptions have to be made though:
\begin{itemize}
\item We will assume the distortion source is a current source
\item To determine the PSD of the distortion introduced in this current source, we will assume that the BLA equals the small signal FRFs. 
\end{itemize}

\begin{figure}
\begin{center}
	\includegraphics[width=0.8\columnwidth]{Prentjes/ACstageFRF.pdf}
\end{center}
\caption{AC simulation set-up to determine the FRF of each stage.}
\label{fig:ACtransferfunctions}       
\end{figure}

Since we assume the distortion source is a current source, we have to transfer the distortion from the output node of the stage into the assumed current source. Therefore, we need to know the FRF from the assumed current source to the output node of the stage. This FRF can be obtained by performing a set of two AC analyses. First, we determine the small-signal FRF of the stage. This is done using the AC simulation set-up shown in Figure~\ref{fig:ACtransferfunctions}. 
\begin{equation*}
{FRF}_{ac,1}=\nicefrac{I_{int,ac}}{V_{amp,ac}}
\end{equation*}
Where $I_{int,ac}$ and $V_{amp,ac}$ are the results of the AC simulation at the nodes shown in Figure~\ref{fig:ACtransferfunctions}. Second, we place a current source at the node where it is assumed and perform another AC simulation (as shown in Figure~\ref{fig:ACoutputrefer}~(a)). The small-signal amplitude of the current source is set to 1. The FRF from current source to the output node of the stage is now given by
\begin{equation*}
F_1 = I_{int,s_1} - {FRF}_{ac,1} \cdot V_{amp,s_1}
\end{equation*}
$I_{int,s_1}$ and $V_{amp,s_1}$ are the results of the AC analysis shown in Figure~\ref{fig:ACoutputrefer}. The PSD of the distortion in the assumed current source is finally given by
\begin{equation*}
\begin{split}
G_{s_1 @ source} = \left( \nicefrac{1}{F_1} \right) \cdot G_{S,1}
\end{split}
\end{equation*}
To obtain the distortion introduced by the second stage in a current source at its output, similar formula's should be used. The AC simulation set-up needed for the second stage is shown in Figure~\ref{fig:ACoutputrefer}~(b)\\

\begin{figure}
\begin{center}
	\includegraphics[width=0.8\columnwidth]{Prentjes/outputrefer.pdf}
\end{center}
\caption{AC simulation set-up to obtain the necessary FRFs to determine the amount of distortion in the current source and to refer the distortion to the output. (a) Stage 1 (b) Stage 2}
\label{fig:ACoutputrefer}
\end{figure}

Now that we have the PSD of the distortion introduced by a stage in a current noise source placed at the stage's output, we need to refer the contribution to the output. Therefore we need the FRF between the noise source and the output node of the op-amp. We will assume that the FRF between the distortion source and the output is also a small-signal FRF. The needed FRF can hence be determined with another AC simulation, where the current source is placed at its location and its influence to the output node is determined. The simulation set-up shown in Figure~\ref{fig:ACoutputrefer} contains an AC current source at the right place already, so by saving the output node there, the right FRF is obtained. The distortion contribution can then be referred to the output by multiplying the found PSD of the distortion in the current source with this FRF
\begin{equation*}
G_{s_1 @ output} = G_{S_1 @ source} \cdot {FRF}_{S_1 \rightarrow out}
\end{equation*}
Again, the strategy is the similar for the second stage.

\subsection{Summary}
\label{sec:Summary}

To find the dominant source of non-linear distortion in a multi-stage op-amp, one has to

\begin{enumerate}
\item Generate a random-odd random-phase multisine that resembles the class of signals for which the op-amp will be used.
\item Perform a transient simulation of the system under test when it is excited with the multisine, save the signals at the input and output of the stages of interest.
\item Transform the results of the transient simulation to the frequency domain and determine the BLA
\item Determine the non-coherent contributions $G_S$ introduced by every stage using spectral correction with the BLA.
\item Perform an AC simulation of the op-amp to determine the AC FRF of every stage.
\item For every stage, perform an AC analysis with an AC current source placed at the output of the stage. This gives the factor $F_i$ needed to transform the non-coherent contributions into the current source and the ${FRF}_{S_i \rightarrow out}$ needed to refer those contributions to the output.
\item Divide the obtained non-coherent contributions $G_{S,i}$ by the found $F_i$
\item Multiply the result with ${FRF}_{S_i \rightarrow out}$ to obtain the output referred non-linear contribution.
\end{enumerate}

\section{Example 1: Miller op-amp}
\label{sec:Miller}
% show the results of the method when applied to the miller op-amp

\begin{figure}
\begin{center}
  \includegraphics{Prentjes/MillerOpamp.pdf}
  \end{center}
\caption{The Miller op-amp under test. Stage 1 and 2 are shown in red and blue respectively}
\label{fig:MillerOpamp}
\end{figure}

As a first example, the BLA-based noise analysis is applied to the Miller op-amp shown in Figure~\ref{fig:MillerOpamp}. The op-amp is designed for the UMC.18 CMOS technology. High voltage transistors were used which allows a supply voltage of 3,3V. During the simulations, a BSIM3v3 model is used for the MOSFETs. The op-amp under test has a Gain Bandwidth product (GBW) of $45\mathrm{MHz}$ and  a  DC  gain  of  $100\mathrm{dB}$.  It is connected as an inverting amplifier with a gain of $5$.\\

The random-odd random-phase multisine used for the experiment has a frequency resolution of $2\mathrm{kHz}$ and excites frequencies up to $10\mathrm{MHz}$. The fixed sample frequency of the transient simulation is $100\mathrm{MHz}$. The 10 multisines with the best crest factor\footnote{The Crest Factor (CF) is the ratio beween the maximum amplitude of a signal divided by its power. \\
$\mathrm{CF}(S) = \nicefrac{\mathrm{max}(\mathrm{abs}(S))}{\mathrm{rms}(S)}$} from a set of 100 random phase realizations were used during the transient simulations. Simulations were performed in Agilent's Advanced Design System (ADS). One period of the multisine with the system in steady-state was simulated and exported to MATLAB for the post-processing. The spectra obtained during the analysis are shown in Figure~\ref{fig:MillerSpectra}.\\

\begin{figure}%
\begin{center}
\includegraphics[width=0.5\columnwidth]{Prentjes/Miller/aMs.pdf}%
\includegraphics[width=0.5\columnwidth]{Prentjes/Miller/aAmp.pdf}\\
\includegraphics[width=0.5\columnwidth]{Prentjes/Miller/aInt.pdf}%
\includegraphics[width=0.5\columnwidth]{Prentjes/Miller/aOut.pdf}%
\end{center}
\caption{Spectra obtained during the transient simulations with a multisine excitation. The even frequency lines, which represent the even-order distortion in the circuit, are shown in blue, the excited odd frequency lines are shown in black and the unexcited odd frequency lines, which represent the amount of odd-order distortion, in red.}
\label{fig:MillerSpectra}
\end{figure}

With these spectra, we calculate the BLA of the stages at excited frequency lines of the multisine by averaging over the different phase realisations. We then use linear interpolation on the BLA to obtain an estimate of the BLA at the non-excited frequency lines and perform the spectral correction (\ref{eq:spectralcorrection}) on the non-excited frequency lines of the output spectrum of each stage. The uncertainty on the BLA of stage 1 is quite large at low frequencies (Figure~\ref{fig:MillerBLAvsAC}).  This is due to the fact that, at the op-amp input, the excitation signal is at the same level as the distortion. To obtain a sufficiently accurate estimate of the BLA for the spectral correction, averaging over 10 realisations of the multisine was needed.\\

%
\begin{figure}%
\includegraphics[width=0.5\columnwidth]{Prentjes/Miller/BLAvsACstage1}%
\includegraphics[width=0.5\columnwidth]{Prentjes/Miller/BLAvsACstage2}%
\caption{Comparison between the BLA ($\bullet$) and the AC analysis (red line). The difference for both stage 1 and stage 2 is less than $0.5\mathrm{dB}$, so the compression term can easily be neglected. The standard deviation on the BLA is shown with the black line}%
\label{fig:MillerBLAvsAC}%
\end{figure}
%

The AC simulations were performed on a logarithmic frequency grid with 50 frequency points per decade. The AC analysis results were then interpolated to obtain values for the frequency response on all the unexcited frequency lines of the multisine. The result was used on the found non-coherent contributions as was explained in section \ref{sec:theory} to refer the contributions to the output.\\

The dominant source of non-linear distortion here is the first stage. The output referred non-linear distortion of every stage is shown in Figure~\ref{fig:MillerOutputReferred}. Blue symbols show the contributions at even frequency lines, representing the even-order non-linear distortion. Red symbols represent the non-excited odd frequency lines, representing the odd non-linear distortion (see section \ref{sec:multisine}). The non-linear distortion generated by the second stage increases, but its distortion level never surpasses the contribution of the first stage. Note that the second stage creates mainly even-order distortion, which is expected for a common-source amplifier \cite{Wambacq1998}.

\begin{figure}%
\includegraphics[width=0.5\columnwidth]{Prentjes/Miller/stage1atOut.pdf}%
\includegraphics[width=0.5\columnwidth]{Prentjes/Miller/stage2atOut.pdf}%
\caption{Output referred non-linear distortion of each stage in the op-amp.}%
\label{fig:MillerOutputReferred}%
\end{figure}

\subsection{Speeding up the analysis}
\label{subsec:SpeedingUp}

The transient simulations are the most time-consuming part of the method. A full period of each realisation of the multisine with the system in steady-state has to be calculated. To obtain a good estimate of the BLA at low frequencies, several realisations are needed. The AC analyses used in the analysis can be calculated on a coarse logarithmic frequency grid and interpolated without introducing significant errors, so the simulation time there is negligible.\\

A big speed-up can be obtained if the op-amp under test behaves dominantly linear. Instead of calculating the BLA, we ignore the compression term $G_B$ in (\ref{eq:BLA}) and use only the underlying linear system $G_0$. In our case, the difference between the BLA and the AC FRF is less than $0.5\mathrm{dB}$ (Figure~\ref{fig:MillerBLAvsAC}). Hence, without great loss of accuracy, the AC FRF can be used in (\ref{eq:spectralcorrection}) instead of the BLA. This means only one transient simulation is necessary to find the dominant source of non-linear distortion.\\

The results of the speedy method are almost exactly the same as the BLA-based method. The error even decreases at low frequencies, because a more accurate estimation of the BLA is obtained. This faster method should be used cautiously though: once the distortion in the circuit becomes stronger and the compression term becomes significant, the BLA should be used.

\section{Example 2: Folded-Cascode op-amp}
\label{sec:FoldedCasc}
% apply the fast method to a Folded-Cascode op-amp
% possibly go into the second stage and determine the dominant source there

\begin{figure}%
\begin{center}
\includegraphics{Prentjes/FoldedCasc/archit.pdf}%	
\end{center}
\caption{Schematic of the folded-cascode op-amp. The second stage consists of a level-shifter and a common-source amplifier.}%
\label{fig:FoldedCasc}%
\end{figure}

As a second example, the method will be applied to the Folded-cascode op-amp shown in Figure~\ref{fig:FoldedCasc}. Again, the op-amp was designed for the UMC.18 technology with a supply voltage of 3.3V. The GBW of this op-amp is $35\mathrm{MHz}$ with a DC gain of $140\mathrm{dB}$. A random-odd random-phase multisine was used as the excitation signal. It excited frequency lines up to $10\mathrm{MHz}$ with a frequency resolution of $2\mathrm{kHz}$. The fixed sample frequency of the transient simulation was set at $100\mathrm{MHz}$ and again one period of the multisine with the system in steady state was simulated.\\

The obtained spectra during the transient analysis are similar to the ones found for the Miller op-amp. The op-amp behaves dominantly linear, so the fast method can be used. Like before, the first stage is the dominant source of non-linear distortion at low frequencies. Now, at high frequencies, the second stage produces as much even-order distortion as stage 1 (Figure~\ref{fig:FoldedCascOutputReferred}).

\begin{figure}%
\includegraphics[width=0.5\columnwidth]{Prentjes/FoldedCasc/FoldedCascStage1atout.pdf}%
\includegraphics[width=0.5\columnwidth]{Prentjes/FoldedCasc/FoldedCascStage2atout.pdf}%
\caption{Output referred non-linear contributions of the first and second stage of the Folded-cascode op-amp.}%
\label{fig:FoldedCascOutputReferred}%
\end{figure}

The second stage in the folded-cascode op-amp consists of a level shifter and a common-source amplifier. This means that it can be considered as a cascade of two stages again and that the BLA-based noise analysis can be applied to this sub circuit. The results show that the common-source amplifier is the dominant source of distortion in the sub-circuit.

\section{Conclusion}
\label{sec:conclusion}

A transient simulation using a multisine excitation allows to determine the Best Linear Approximation (BLA) of a circuit. The BLA considers the non-linear system as a linear system where the non-linear distortion generated by the system is modelled as noise added to the output of the system. Because the distortion behaves as noise, we can apply a noise analysis on the several distortion sources in the circuit to determine the dominant source of non-linear distortion.\\

This BLA-based noise analysis was successfully applied to a two-stage Miller op-amp and a two-stage Folded-cascode op-amp. The method can be applied hierarchically to determine the dominant source of distortion in a sub network. For dominantly linear circuits like these op-amps, the BLA-based analysis requires only one transient simulation and a few quick AC analyses. It does not require special simulation techniques or models.

\section{Future work}
\label{sec:FutureWork}

The BLA-based noise analysis in its current shape relies on the assumption that the non-linear distortion can be referred to the output with a small-signal FRF. This may be true for the op-amps under consideration, but if the second stage were a class AB output stage for example, this assumption would not hold. More research is needed to be able to refer the distortion to the output in such cases.

% BibTeX users please use one of
\bibliography{PaperDatabase}
\bibliographystyle{spmpsci}      

\end{document}

% For one-column wide figures use
%\begin{figure}
% Use the relevant command to insert your figure file.
% For example, with the graphicx package use
%  \includegraphics{example.eps}
% figure caption is below the figure
%\caption{Please write your figure caption here}
%\label{fig:1}       % Give a unique label
%\end{figure}

% For two-column wide figures use
%\begin{figure*}
% Use the relevant command to insert your figure file.
% For example, with the graphicx package use
%  \includegraphics[width=0.75\textwidth]{example.eps}
% figure caption is below the figure
%\caption{Please write your figure caption here}
%\label{fig:2}       % Give a unique label
%\end{figure*}

% For tables use
%\begin{table}
% table caption is above the table
%\caption{Please write your table caption here}
%\label{tab:1}       % Give a unique label
% For LaTeX tables use
%\begin{tabular}{lll}
%\hline\noalign{\smallskip}
%first & second & third  \\
%\noalign{\smallskip}\hline\noalign{\smallskip}
%number & number & number \\
%number & number & number \\
%\noalign{\smallskip}\hline
%\end{tabular}
%\end{table}


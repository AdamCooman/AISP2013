clear all
close all

%% random gaussian noise
perlength=200;
numperiod=100;
time=(0:numperiod*perlength-1)/(numperiod*perlength);

% real noise
x_noise=randn(perlength*numperiod,1);
x_noise=x_noise-mean(x_noise);

% periodic noise
x_pernoise=randn(perlength,1);
x_pernoise=x_pernoise-mean(x_pernoise);
x_pernoise=repmat(x_pernoise,numperiod,1);

% multisine
freqbins=zeros(perlength,1);
phase=2*pi*rand(round(0.5*perlength),1);
freqbins(1:1+length(phase)-1)=cos(phase)+1i*sin(phase);

x_multisine=real(ifft(freqbins));
x_multisine=x_multisine-mean(x_multisine);
x_multisine=repmat(x_multisine,numperiod,1);

% give all the signals the same RMS value
rms_noise=sqrt(mean(x_noise.^2));
rms_pernoise=sqrt(mean(x_pernoise.^2));
rms_multisine=sqrt(mean(x_multisine.^2));

rms_wanted=1000;
foefelfactor=10^(-15/20);

x_noise=foefelfactor*rms_wanted*x_noise/rms_noise;
x_pernoise=rms_wanted*x_pernoise/rms_pernoise;
x_multisine=rms_wanted*x_multisine/rms_multisine;

amp=max(db(fft(x_multisine)/numperiod*2));

% filter the signals to obtain non-white excitation signal
% b = fir1(10,pi/6);
% f = [0 0.6 0.6 1]; m = [1 1 0 0];
% b = fir2(30,f,m);

[b,a]=butter(3,0.4);
[hn,~] = freqz(b,a,length(x_noise)/2);
[hp,~] = freqz(b,a,perlength/2);
% plot(f,db(m),w/pi,db(hn))

x_noise=filter(b,a,x_noise);
x_pernoise=filter(b,a,x_pernoise);
x_multisine=filter(b,a,x_multisine);

% go to the frequency domain

s_noise=fft(x_noise);
s_pernoise=fft(x_pernoise)/numperiod*2;
s_multisine=fft(x_multisine)/numperiod*2;

%% make the plot with signal, distribution and spectrum
close all
figure('name','excitation signals')

timeblue=perlength+1-perlength/2:2*perlength+perlength/2;
timered =perlength+1:2*perlength;

nbins=10;
histxlimits=[-2000 2000];
specylimits=[0 120];
specxlimits=[0 80];
specnoisexlimits=[0 80*numperiod];

% random noise
subplot(331)
plot(time(timeblue),x_noise(timeblue),'b')
% ylim(histxlimits)
set(gca, 'XTickLabelMode', 'manual', 'XTickLabel', []);
set(gca, 'YTickLabelMode', 'manual', 'YTickLabel', []);

subplot(332)
histfit(x_noise(1:perlength),nbins)
xlim([-300 300])
set(gca, 'XTickLabelMode', 'manual', 'XTickLabel', []);
set(gca, 'YTickLabelMode', 'manual', 'YTickLabel', []);

subplot(333)
plot(db(s_noise(1:end/2)),'.')
hold on
plot(db(hn)+amp,'r','LineWidth',2)
ylim(specylimits)
xlim(specnoisexlimits)
set(gca, 'XTickLabelMode', 'manual', 'XTickLabel', []);
set(gca, 'YTickLabelMode', 'manual', 'YTickLabel', []);

% periodic noise
subplot(334)
plot(time(timeblue),x_pernoise(timeblue),'b')
ylim(histxlimits)
hold on
plot(time(timered),x_pernoise(timered),'r')
set(gca, 'XTickLabelMode', 'manual', 'XTickLabel', []);
set(gca, 'YTickLabelMode', 'manual', 'YTickLabel', []);

subplot(335)
histfit(x_pernoise(1:perlength),nbins)
xlim(histxlimits)
set(gca, 'XTickLabelMode', 'manual', 'XTickLabel', []);
set(gca, 'YTickLabelMode', 'manual', 'YTickLabel', []);

subplot(336)
stem(db(s_pernoise(1:numperiod:end/2)),'.')
hold on
plot(db(hp)+amp,'r','LineWidth',2)
ylim(specylimits)
xlim(specxlimits)
set(gca, 'XTickLabelMode', 'manual', 'XTickLabel', []);
set(gca, 'YTickLabelMode', 'manual', 'YTickLabel', []);

% multisine
subplot(337)
plot(time(timeblue),x_multisine(timeblue),'b')
ylim(histxlimits)
hold on
plot(time(timered),x_multisine(timered),'r')
set(gca, 'XTickLabelMode', 'manual', 'XTickLabel', []);
set(gca, 'YTickLabelMode', 'manual', 'YTickLabel', []);

subplot(338)
histfit(x_multisine(1:perlength),nbins)
xlim(histxlimits)
set(gca, 'XTickLabelMode', 'manual', 'XTickLabel', []);
set(gca, 'YTickLabelMode', 'manual', 'YTickLabel', []);

subplot(339)
stem(db(s_multisine(1:numperiod:end/2)),'.')
hold on
plot(db(hp)+amp,'r','LineWidth',2)
ylim(specylimits)
xlim(specxlimits)
set(gca, 'XTickLabelMode', 'manual', 'XTickLabel', []);
set(gca, 'YTickLabelMode', 'manual', 'YTickLabel', []);

